import java.util.Scanner;
public class Lottery {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		// Prompt the user to enter two guess numbers as  integers
		System.out.print("Enter the Guess Numbers(two pick digits): ");
		int GuessNumber = input.nextInt();
		
		//Generate a lottery number
		int Lottery = (int)(Math.random() * 100);
		
		//Get digits from the Lottery
		int LotteryNumber1 = Lottery / 10;
		int LotteryNumber2 = Lottery % 10;
		
		//Get digits from the GuessNumber
		int GuessNumber1 = GuessNumber /10;
		int GuessNumber2 = GuessNumber % 10;
		
		System.out.println("The Lottery Number is " + Lottery);
		
		//Check the attribute of the guess
		if (GuessNumber == Lottery)
			System.out.println("Exact Match: you win $10,000");
		else if (GuessNumber2 == LotteryNumber1 && GuessNumber1 == LotteryNumber2)
			System.out.println("Match all digit numbres: you win $3,000");
		else if (GuessNumber1 == LotteryNumber1
				|| GuessNumber1 == LotteryNumber2
				||GuessNumber2 == LotteryNumber1
				|| GuessNumber2 == LotteryNumber2)
			System.out.println("Match one digit number: you win $1,000");
		else
			System.out.println("Sorry, no match");
			
		

	}

}
