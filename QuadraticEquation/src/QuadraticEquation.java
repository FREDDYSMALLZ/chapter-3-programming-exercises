import java.util.Scanner;
public class QuadraticEquation {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter the values of a, b and c
		System.out.print("Enter the values of a, b, and c :");
		 double a = input.nextDouble();
		 double b = input.nextDouble();
		 double c = input.nextDouble();
		 
		 double discriminant = Math.pow(b, 2) - 4 * a * c;
		 double r1 = (-b + Math.sqrt(discriminant)) / 2 * a;
		 double r2 = (-b - Math.sqrt(discriminant)) / 2 * a;
		 
		 if( discriminant < 0)
			 System.out.println("The equation has no real roots.");
		
		 
		 if(discriminant > 0)
			 System.out.println("The equation has two real roots." + r1 + " and " + r2);
		 
		 else
			 System.out.println("The equation has one root" + r1);
		 
	}

}
