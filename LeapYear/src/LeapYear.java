import java.util.Scanner;
public class LeapYear {

	public static void main(String[] args) {
		// Create a new scanner input
		Scanner input = new Scanner(System.in);
		
		//Prompt the user to enter the year as an integer number
		System.out.print("Enter a year: ");
		
		int year = input.nextInt();
		
		//Check if the year is a leap year
		boolean isLeapYear = (year % 4 == 0 && year % 100 != 0)||
				(year % 400 == 0);
		//Display results to the console
		System.out.println( "Is " + year + " a Leap Year? " + isLeapYear);

	}

}
