import java.util.Scanner;

public class LinearEquation {

	public static void main(String[] args) {
		//solve an equation using creamers rule
		
		Scanner input =new Scanner(System.in);
		// prompt the user to enter the values of a,b,c,d,e and f
		System.out.print("Enter the values of a, b, c, d, e, and f: ");
		double a = input.nextDouble();
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();
		
		double x = ( e * d - b * f ) / ( a * d - b * c );
		double y = ( a * f - e * c ) / ( a * d -  b * c );
		
		//Display results to the console
		if (a * d - b * c == 0){
			System.out.println("The Equation has no solution.");
			System.exit(0);
		
		System.out.println("x = " + x + " and " + " y = " + y);
		}
	}

}
